# actev validate-execution

## Description

Checks the structure of the  directory after ActEV-system-setup is run. Checks for global directory structure, expected API contents, and system outputs directory and integrity.
The `--strict` flag causes the command to throw an exception on an error. Otherwise only WARNING logs are displayed.

In order to succed:
 * The repository contains the required sub-directories,
 * The signatures of the .src.** functions have to remain unchanged,
 * The container_outputs directory contains at least one system output,
 * Each reported system output is different from the baseline's,
 * Names of the directories inside `container_outputs/` follow a strict syntax < dataset_name >\_*
 * Each subdirectory contains a set of 4 files:
   * < dataset_name >\_activity.json
   * < dataset_name >\_chunk.json
   * < dataset_name >\_file.json
   * < dataset_name >\_output.json

This command requires the following parameters:

## Parameters

| Name      | Id         | Required | Definition                 |
|-----------|------------|---------|----------------------------|
| strict         | --strict | False    | Whether to throw an exception on an error.    |

## Usage

```
actev validate-system
```

Example:

```
actev validate-system --strict
```
