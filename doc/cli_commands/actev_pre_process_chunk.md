# actev pre-process-chunk

## Description

Pre process a chunk

This command requires the following parameters:

## Parameters

| Name      | Id         | Required | Definition                 |
|-----------|------------|---------|----------------------------|
| chunk-id            | i | True    | chunk id                         |
| system-cache-dir    | s | False    | path to system cache directory   |


## Usage

Generic command:

```
actev pre-process-chunk -i <id of a chunk> -s <path to system cache directory>
```

Example:

```
actev pre-process-chunk -i Chunk1 -s ~/system_cache_dir
```