"""Entry point module: experiment-cleanup

Implements the entry-point by using Python or any other languages.
"""

import os


def entry_point(system_cache_dir):
    """Method to complete: you have to raise an exception if an error occured
    in the program.

    Close any servers, terminates cluster (future functionality), etc.

    Args:
        system_cache_dir (str): Path to system cache directory

    """
    # raise NotImplementedError("You should implement the entry_point method.")
    path = os.path.dirname(os.path.abspath(__file__))
    path = os.path.join(system_cache_dir, 'data_mcprl')
    status = os.system("rm -rf {}".format(path))
    if status != 0:
        raise Exception("Error occured in experiment-cleanup")
